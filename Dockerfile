FROM python:3.10
EXPOSE 6969
WORKDIR /code
COPY requirements.txt requirements.txt
RUN python3 -m pip install -r requirements.txt
COPY . .

# Command line arguments will be overwritten by enviroment variables is they exist.
# This is intended behaviour.
CMD ["python", "-u", "app.py"]