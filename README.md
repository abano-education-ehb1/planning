# Integration Project 22: Planning

Created by Ryan De Vogel, Arthur Van Remoortel and Jean-Christophe Hondo.

---
## Configuration
Configuration is passed to the `flask` server using environment variables stored a `.env` file which is read whenever the container starts. This way you can easily change configuration without rebuilding the docker images.

The `.env` file contains secret information and should not be added on `git`. Simply create your own `.env` file locally by copying the `.env.example` and make your changes.

    cp .env.example .env

The .env file contains these options:

    RABBITMQ_IP:        RabbitMQ server address.
    RABBITMQ_PORT:      RabbitMQ server port.
    RABBITMQ_USERNAME:  Login username.
    RABBITMQ_PASSWORD:  Login password.
    
    DB_USER:            MySQL database user.
    DB_PASSWORD:        MySQL database password.
    DB_HOST:            MySQL database host or ip.
    DB_PORT:            MySQL database port.

    USE_GOOGLE=True     Can be used to disable the Google API. Usefill for development and testing without overusing the API.

## Running with Docker-Compose
IMPORTANT: You will be required to run the server locally from python the very first time on order to generate the Google authentication token. Afterwards:

Run docker-compose: `docker-compose up`

---
## Running locally
### Installation
    python -m pip install -r requirements.txt
You will be required to set up a mysql database yourself and configure it in the `.env` file

Check out the [Credentials](#Credentials) section for the final step.
### Running
    python app.py
The first time you will be required to authenticate your Google account. Follow the [instructions below.](#Google Calendar Credentials) A more detailed version is available on [confluence](https://abano-education-ehb.atlassian.net/wiki/spaces/IP2/pages/393740611/Credentials#Google-Calendar-Credentials).

## Credentials

### Google Calendar Credentials
This step needs to be completed on a device with a browser. It does not need to be connected to the EhB network.
Run the server using `python app.py`.
1) Open the URL in the browser. 
2) Log in using the [Google credentials](https://abano-education-ehb.atlassian.net/wiki/spaces/IP2/pages/393740611/Credentials#Google) for this project.
3) Complete the authentication flow. The
`credentials.json` and `token.json` are 2 more files required to run the server and **must be placed in the root directory**.
4) Optional: Copy the updated `token.json` to 

All credentials and Google API token setup can be found on [Confluence](https://abano-education-ehb.atlassian.net/wiki/spaces/IP2/pages/393740611/Credentials#Google-Calendar-Credentials)