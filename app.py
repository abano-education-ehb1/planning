from config import RABBITMQ_HOST, RABBITMQ_PORT
from inititalisation import app, google_calendar_service
from consumers.sessions_consumer import subscribe_session_consumers
from consumers.user_creation_consumer import subscribe_user_consumers
from logger import Log
from models import db
from publishers.monitoring_publisher import start_monitoring_publisher
# Make sure to import the models here so db.create_all() detect it and creates a table if it does not exist.
import models.user
import models.session

@app.route('/')
def index():
    return 'OK'


@app.route('/test/<cmd>')
def test_route(cmd):
    """ Used to test if integration between flask, database and rabbitmq works.
    Will send the value of cmd -> a RabbitMQ queue -> do something on the database.
    :param cmd: value from url as string.
    """
    return "disabled"


if __name__ == '__main__':
    print(f"Running Planning Server: (RabbitMQ = {RABBITMQ_HOST}:{RABBITMQ_PORT})")
    if google_calendar_service is None:
        e = Exception("Cannot start Planning Server due to failed google_calendar_service initialization.")
        Log().log_critical(str(e))
        raise e
    db.init_app(app)
    with app.app_context():
        db.create_all()
        print("Created database tables.")
    # Log(use_rabbitmq=True).log_info("Started...")
    subscribe_user_consumers()
    subscribe_session_consumers()
    start_monitoring_publisher(start_delay=2)  # Delayed a bit so other connections can be created first.

    app.run(debug=True, host='0.0.0.0', port=5000, use_reloader=False)
