import logging
import os

import pika
from dotenv import load_dotenv

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))


# Command line arguments are overwritten by environment variables if they exist.
# Environment variables ar used when running the server through docker-compose.
# parser = argparse.ArgumentParser()
# parser.add_argument("--ip", default='10.3.56.3', help="RabbitMQ server IP.")
# parser.add_argument("--port", default=5672, help="RabbitMQ server port.")
# parser.add_argument("--user", default='guest', help="RabbitMQ user.")
# parser.add_argument("--password", default='guest', help="RabbitMQ password.")
# parser.add_argument("--db-user", default='root', help="Database username.")
# parser.add_argument("--db-password", default='guest', help="Database password.")
# parser.add_argument("--db-host", default='localhost', help="Database hostname/ip.")
# parser.add_argument("--db-port", default=3306, help="Database port.")
# parser.add_argument("--ignore-google", action="store_true", help="Disable Google API.")
# args, unknown = parser.parse_known_args()

# RABBITMQ_HOST = args.ip
# RABBITMQ_PORT = args.port
# RABBITMQ_USERNAME = args.user
# RABBITMQ_PASSWORD = args.password
#
# DB_USER = args.db_user
# DB_PASSWORD = args.db_password
# DB_HOST = args.db_host
# DB_PORT = args.db_port
#
# # Enable/Disable Google API during development or testing to avoid reaching the API limit.

# Overwrite the command line arguments with environment variables if they exist.
load_dotenv()
RABBITMQ_PORT =  os.getenv('RABBITMQ_PORT', default=5672)
RABBITMQ_HOST = os.getenv('RABBITMQ_IP', default="localhost")
RABBITMQ_USERNAME = os.getenv('RABBITMQ_USERNAME', default="guest")
RABBITMQ_PASSWORD = os.getenv('RABBITMQ_PASSWORD', default="guest")
DB_USER = os.getenv('DB_USER')
DB_PASSWORD = os.getenv('DB_PASSWORD')
DB_HOST = os.getenv('DB_HOST')
DB_PORT = os.getenv('DB_PORT')

USE_GOOGLE = os.getenv('USE_GOOGLE', default='True').lower() == "true"
GOOGLE_EMAIL = os.getenv('GOOGLE_EMAIL')
# Constants
DATABASE_NAME = 'planning'
USER_QUEUE_NAME = 'planning_user_queue'
HEARTBEAT_QUEUE = "monitoring"
UUID_QUEUE = "uuid_manager"
SESSION_QUEUE_NAME = 'planning_session_queue'

# Initialisation
rabbitmq_credentials = pika.PlainCredentials(RABBITMQ_USERNAME, RABBITMQ_PASSWORD)
rabbitmq_parameters = pika.ConnectionParameters(
    host=RABBITMQ_HOST,
    port=RABBITMQ_PORT,
    credentials=rabbitmq_credentials
)
MIN_GOOGLE_ACTION_SLEEP = 2  # Minimum delay between google API calls.
MAX_GOOGLE_ACTION_SLEEP = 64  # Sleep will never go higher than this.

LOGGER_FORMAT = '%(asctime)s %(levelname)s: %(message)s'
logging.basicConfig(filename='planning.log',
                    format=LOGGER_FORMAT,
                    )

flask_logger = logging.getLogger('werkzeug')
flask_logger.setLevel(logging.ERROR)  # Disables logging from flask except ERROR.
