from collections import deque
import config


class Consumer:
    # TODO: Refactor SessionsConsumer and UserConsumer.
    def __init__(self):
        self.channel = None
        self.google_action_queue: deque = deque()
        self.google_action_sleep: int = config.MIN_GOOGLE_ACTION_SLEEP


    def enqueue_google_user_action(self, action: str, object_id: int, arguments=None, front=False):
        """
        Puts google actions on a double ended queue. They can be executed later.
        :param action: string that identifies the action. (create, update, delete, ...)
        :param object_id: the database ID of the related object if relevant. (User ID for creating calendars, Session ID google events, ...)
        :param arguments: Optional list of arguments required for the action.
        :param front: Insert at the front of the queue.
        """
        if arguments is None:
            arguments = []
        if front:
            self.google_action_queue.appendleft((action, object_id, arguments))
        else:
            self.google_action_queue.append((action, object_id, arguments))