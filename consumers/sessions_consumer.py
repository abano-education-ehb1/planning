import datetime
import time
import pika.exceptions
from pprint import pprint
from threading import Thread
from xml.etree import ElementTree

from sqlalchemy import and_

import config
import google_calendar
import googleapiclient.errors
from unittest import mock

import inititalisation
import pika

from consumers import Consumer
from helpers import (dictionary_from_xml, prettify_xml, string_to_datetime,
                     xml_from_string, find_xml_subtree, xml_to_string, timestamp_to_datetime, reconnect_on_fail)
from lxml import etree

from logger import Log
from models import db
from models.session import Session, Sessions_Users
from models.user import User

uuid_manager_response_template = """
<response>
    <source>planning</source>
    <source-id>{}</source-id>
    <uuid>{}</uuid>
    <entity>session</entity>
    <action>{}</action>
</response>
"""


class SessionConsumer(Consumer):
    channel = None

    def __init__(self):
        super().__init__()
        # Search for sessions without and google ID and tries to create them again.
        if config.USE_GOOGLE:
            with inititalisation.app.app_context():
                failed_sessions = Session.query.filter(Session.google_event_id == None).all()
                failed_session: Session
                for failed_session in failed_sessions:
                    Log().log_info(f"{failed_session} not on Google calendar. Will retry now.")
                    self.enqueue_google_user_action(action="create", object_id=failed_session.id)

                failed_personal_sessions = Sessions_Users.query.filter(Sessions_Users.personal_google_event_id == None).all()
                failed_session: Sessions_Users
                for failed_session in failed_personal_sessions:
                    Log().log_info(f"{failed_session.session} for {failed_session.user} 'has no calendar. Will retry now.")
                    self.enqueue_google_user_action(action="create-participant", object_id=failed_session.session_id, arguments=[failed_session.user_id])
        Thread(target=self.start_google_action_processor, daemon=True).start()

    def start_consumer(self):
        """
        Creates the rabbitMQ connection and starts consuming. Blocks the current thread
        """
        while True:
            """
            Tries to reconnect when connection is lost to rabbitMQ
            """
            try:
                self.setup_rabbitmq_connection()
                self.channel.start_consuming()
            except (pika.exceptions.ConnectionClosedByBroker, pika.exceptions.AMQPConnectionError, pika.exceptions.AMQPChannelError) as e:
                Log().log_error(f"RabbitMQ connection closed on queue {config.SESSION_QUEUE_NAME}: {e}. Retrying connection in 5 seconds.")
                time.sleep(5)

    def setup_rabbitmq_connection(self):
        """
        Creates the rabbitMQ connection. Can also be used to recreate the connections if it breaks.
        """
        Log().log_info(f'Creating new connection to queue {config.SESSION_QUEUE_NAME}')
        connection = pika.BlockingConnection(config.rabbitmq_parameters)
        self.channel = connection.channel()
        self.channel.queue_declare(queue=config.SESSION_QUEUE_NAME)
        self.channel.basic_consume(on_message_callback=self.callback, queue=config.SESSION_QUEUE_NAME, auto_ack=True)

    def callback(self, channel, method, properties, body):
        """
        Function is called every time data arrives on the current queue.
        :param body: content of the rabbitMQ message (in our case the XML)
        """
        if isinstance(body, str):
            data = body
        else:
            data = body.decode()
        if "<source>planning</source>" in data:
            # Ignore when queue receives data sent from itself.
            return
        try:
            xml_temp, _ = xml_from_string(data, validate=False)
        except Exception as e:
            Log().log_error(f"SessionConsumer.callback: Could not parse xml={data}")
            return
        action = find_xml_subtree(xml_temp, 'action', only_first=True).text
        entity = xml_temp.tag

        Log().log_info(f"SessionConsumer received xml: {xml_to_string(xml_temp)}")
        if entity == "session":
            xml, is_valid = xml_from_string(data)
            if not is_valid:
                Log().log_error(f'SessionConsumer.callback: xml is not valid.\n{xml_to_string(xml)}')
                return
            if action == "create":
                return self.create_session(xml=xml)
            elif action == "delete":
                return self.delete_session(xml=xml)
            elif action == "update" in data:
                return self.update_session(xml=xml)
        elif entity == "user-session":
            xml, is_valid = xml_from_string(data)
            if not is_valid:
                Log().log_error(f'SessionConsumer.callback: xml is not valid.\n{xml_to_string(xml)}')
                return
            return self.add_or_remove_participants(xml=xml)
        else:
            Log().log_error(f'SessionConsumer.callback: received an unknown entity.\n{data}')

    def add_or_remove_participants(self, xml)  -> Sessions_Users:
        """
        Add or removes participants (or users) from a given session
        :param xml: xml data contains the data to add or remove participants
        """
        try:
            with inititalisation.app.app_context():
                user_id = find_xml_subtree(xml, 'userId', only_first=True).text
                session_id = find_xml_subtree(xml, 'sessionId', only_first=True).text
                action = find_xml_subtree(xml, 'action', only_first=True).text
                session: Session = Session.query.filter(Session.id == session_id).first()
                user: User = User.query.filter(User.id == user_id).first()
                if not user:
                    Log().log_error(f"User does not exist.")
                    return
                if not session:
                    Log().log_error(f"Session does not exist.")
                    return
                existing_session_user_assoc: Sessions_Users = False
                for assoc in session.participants:
                    if assoc.user == user:
                        existing_session_user_assoc = assoc

                if action == "delete" or action == "Cancel-reservation":
                    if not session:
                        Log().log_error(f"Tried to remove user from session that doesn't exist.")
                        return
                    if not user:
                        Log().log_error(f"Tried to remove user from session who doesn't exist.")
                        return
                    if not existing_session_user_assoc:
                        Log().log_warning(f"Tried to remove user from session that isn't participating'. user={user.uuid}, session={session.uuid}")
                        return
                    if config.USE_GOOGLE:
                        self.enqueue_google_user_action(action='delete-participant', object_id=session.id, arguments=[user.id, existing_session_user_assoc.personal_google_event_id])

                    db.session.delete(existing_session_user_assoc)
                    db.session.commit()

                elif action == "create":
                    Log().log_info(f"Adding {user.id} to {session.id}")
                    if existing_session_user_assoc:
                        Log().log_warning(f"Tried to add user to session that is already participating. user={user.uuid}, session={session.uuid}")
                        return
                    personal_google_event_id = None
                    session_user = session.add_participant(user, None)
                    db.session.commit()

                    if config.USE_GOOGLE:
                        self.enqueue_google_user_action(action='create-participant', object_id=session.id, arguments=[user.id, user.calendar_id])

                    return session_user
        except Exception as e:
            print(f"SessionConsumer.add_participant_to_session: an exception occurred. {e}")

    def create_session(self, xml) -> Session:
        """
        Creates a session from the xml data
        :param xml: xml data contains the data to create a new session
        :return: session
        """
        try:
            with inititalisation.app.app_context():
                begin_datetime_str = int(find_xml_subtree(xml, 'beginDateTime', only_first=True).text)
                end_datetime_str = int(find_xml_subtree(xml, 'endDateTime', only_first=True).text)
                name = find_xml_subtree(xml, 'name', only_first=True).text
                speaker = find_xml_subtree(xml, 'speaker', only_first=True).text
                location = find_xml_subtree(xml, 'location', only_first=True).text
                description = find_xml_subtree(xml, 'description', only_first=True).text
                uuid = find_xml_subtree(xml, 'uuid', only_first=True).text
                session = Session(uuid=uuid,
                                  name=name,
                                  description=description,
                                  begin_time=timestamp_to_datetime(begin_datetime_str),
                                  end_time=timestamp_to_datetime(end_datetime_str),
                                  speaker=speaker,
                                  location=location,
                                  google_event_id=None  # Added later.
                                  )
                db.session.add(session)
                db.session.commit()
                Log().log_info(f'Created new session: {session}')
                if config.USE_GOOGLE:
                    self.enqueue_google_user_action(action='create', object_id=session.id)
                if self.channel:
                    self.send_rabbitmq_response(response_template=uuid_manager_response_template,
                                                action="created",
                                                arguments=[session.id, uuid])
                return session
        except Exception as e:
            Log().log_error(f"SessionConsumer.create_session: {e}")

    def delete_session(self, xml):
        """
        Deletes an existing session
        :param xml: xml data contains the data to delete a session
        """
        try:
            with inititalisation.app.app_context():
                id = find_xml_subtree(xml, 'source-id', only_first=True).text
                uuid = find_xml_subtree(xml, 'uuid', only_first=True).text
                session = Session.query.filter(Session.uuid == uuid).first()
                if not session:
                    Log().log_error(f"Deleted session does not exist.")
                    return
                if config.USE_GOOGLE:
                    self.enqueue_google_user_action(action='delete', object_id=None, arguments=[session.google_event_id])

                # Also delete calendars of all participants.
                for participant_assoc in session.participants:
                    if config.USE_GOOGLE:
                        self.enqueue_google_user_action(action='delete-participant', object_id=None, arguments=[participant_assoc.user.id, participant_assoc.personal_google_event_id])
                    db.session.delete(participant_assoc)

                db.session.delete(session)
                db.session.commit()
                Log().log_info(f'Deleted session: {session}')
                if self.channel:
                    self.send_rabbitmq_response(response_template=uuid_manager_response_template,
                                                action="deleted",
                                                arguments=[session.id, uuid])
        except Exception as e:
            Log().log_error(f"SessionConsumer.delete_session: {e}")

    def update_session(self, xml) -> Session:
        """
        Updates an existing session given the xml data
        :param xml: xml data contains the data to update a session
        """
        try:
            with inititalisation.app.app_context():
                begin_datetime_str = int(find_xml_subtree(xml, 'beginDateTime', only_first=True).text)
                end_datetime_str = int(find_xml_subtree(xml, 'endDateTime', only_first=True).text)
                session_id = find_xml_subtree(xml, 'source-id', only_first=True).text
                uuid = find_xml_subtree(xml, 'uuid', only_first=True).text
                session = Session.query.filter(Session.uuid == uuid)
                session_object: Session = session.first()
                if not session_object:
                    Log().log_error(f"Updated session does not exist. {xml_to_string(xml)}")
                    return
                new_data = {
                    'name': find_xml_subtree(xml, 'name', only_first=True).text,
                    'begin_time': timestamp_to_datetime(begin_datetime_str),
                    'end_time': timestamp_to_datetime(end_datetime_str),
                    'speaker': find_xml_subtree(xml, 'speaker', only_first=True).text,
                    'location': find_xml_subtree(xml, 'location', only_first=True).text,
                    'description': find_xml_subtree(xml, 'description', only_first=True).text,
                }
                session.update(new_data)
                db.session.commit()
                Log().log_info(f'Updated Session: {session_object}')
                if session_object:
                    if config.USE_GOOGLE:
                        self.enqueue_google_user_action(action='update', object_id=session_object.id, arguments=[new_data])
                    # Also update calendars of all participants.
                    for participant_assoc in session_object.participants:
                        if config.USE_GOOGLE:
                            self.enqueue_google_user_action(action='update-participant', object_id=session_object.id,
                                                            arguments=[participant_assoc.user.id,
                                                                       participant_assoc.personal_google_event_id])
                    if self.channel:
                        self.send_rabbitmq_response(response_template=uuid_manager_response_template,
                                                    action="updated",
                                                    arguments=[session_object.id, uuid])
                    return session_object
        except Exception as e:
            Log().log_error(f"SessionConsumer.update_session: {e}")

    def start_google_action_processor(self):
        """
        Loops over action in the google_action_queue and performs them with a delay between them.
        The delay will be doubled when an action failed and halved when it succeeds.
        Should be run in a background thread.
        # TODO: Needs refactoring.
        """
        this_thread_cal_service, this_thread_gmail_service = google_calendar.get_calendar_service()
        # The Google services are not thread safe. Always make a new service in each thread.
        while True:
            failed = False
            if self.google_action_queue:
                with inititalisation.app.app_context():
                    next_action, session_id, arguments = self.google_action_queue.popleft()
                    session = None
                    if session_id:
                        session: Session = Session.query.get(session_id)

                    if next_action == "create" and session:
                        try:
                            user_calendar = google_calendar.create_event(
                                service=this_thread_cal_service,
                                calendar_id=google_calendar.MAIN_CALENDAR_ID,
                                session=session
                            )
                            session.google_event_id = user_calendar['id']
                            db.session.commit()
                            Log().log_info(f"Created Google event for {session}.")
                        except googleapiclient.errors.HttpError as e:
                            if e.status_code == 403:
                                Log().log_warning(
                                    f"Google_action_processor: Create - Google API error for {session}: {e}. Will try again later.")
                                failed = True

                    elif next_action == "update" and session:
                        try:
                            google_calendar.update_event(
                                service=this_thread_cal_service,
                                calendar_id=google_calendar.MAIN_CALENDAR_ID,
                                event_id=session.google_event_id,
                                session=session
                            )
                            Log().log_info(f"Updated Google event for {session}.")
                        except googleapiclient.errors.HttpError as e:
                            if e.status_code == 403:
                                Log().log_warning(
                                    f"Google_action_processor: Create - Google API error for {session}: {e}. Will try again later.")
                                failed = True

                    elif next_action == "delete":
                        google_event_id = arguments[1]
                        if not google_event_id:
                            self.enqueue_google_user_action(action=next_action, object_id=session_id,
                                                            arguments=arguments, front=True)
                            continue
                        if not google_event_id:
                            Log().log_warning(f"Trying to delete google event for {session} failed because google_event_id is None. Expected if the session was deleted to fast.")
                            continue
                        try:
                            google_calendar.delete_event(
                                service=this_thread_cal_service,
                                calendar_id=google_calendar.MAIN_CALENDAR_ID,
                                event_id=google_event_id,
                            )
                            Log().log_info(f"Deleted Google event for {session}")
                        except googleapiclient.errors.HttpError as e:
                            if e.status_code == 403:
                                Log().log_warning(
                                    f"Google_action_processor: Create - Google API error for {session}: {e}. Will try again later.")
                                failed = True

                    elif next_action == "create-participant":
                        user_id = arguments[0]
                        user = User.query.get(user_id)
                        if not user or not session:
                            # User or session has been deleted during delay. Don't create anything.
                            continue

                        if not user.calendar_id:
                            # Joining session before the Google calendar has been created. Try again later.
                            self.enqueue_google_user_action(action=next_action, object_id=session_id,
                                                            arguments=arguments, front=True)
                            continue
                        session_user: Sessions_Users = Sessions_Users.query.filter(
                            and_(Sessions_Users.session_id == session_id, Sessions_Users.user_id == user_id)).first()
                        if not session_user:
                            # Sessions_Users assoc has been deleted during delay. Don't create anything..'
                            continue
                        try:
                            user_event = google_calendar.create_event(service=this_thread_cal_service,
                                                                      calendar_id=user.calendar_id, session=session)
                            personal_google_event_id = user_event['id']
                            session_user.personal_google_event_id = personal_google_event_id
                            db.session.commit()
                            Log().log_info(f"Created Google event for {session} on personal calendar for user({user_id})")
                        except googleapiclient.errors.HttpError as e:
                            if e.status_code == 403:
                                Log().log_warning(
                                    f"Google_action_processor: Create - Google API error for {session}: {e}. Will try again later.")
                                failed = True

                    elif next_action == "delete-participant":
                        user_id = arguments[0]
                        user: User = User.query.get(user_id)
                        personal_google_event_id = arguments[1]
                        if not user:
                            print("--------------------- USER IS NONE")
                            # User has already been deleted.
                            continue

                        if not personal_google_event_id:
                            print("--------------------- personal_google_event_id IS NONE")
                            # Personal calendar does not exist yet.
                            session_user: Sessions_Users = Sessions_Users.query.filter(
                                and_(Sessions_Users.session_id == session_id,
                                     Sessions_Users.user_id == user_id)).first()
                            if not session_user:
                                print("--------------------- session_user IS ALSO NONE")
                                # Sessions_Users is also None.
                                # "delete-participant" called but user left session to fast. Don't need to do anything.
                                continue
                            personal_google_event_id = session_user.personal_google_event_id


                        if not user.calendar_id:
                            # User exists but calendar doesn't exist yet. Retry later.
                            self.enqueue_google_user_action(action=next_action, object_id=session_id,
                                                            arguments=arguments, front=True)
                            continue

                        try:
                            google_calendar.delete_event(
                                service=this_thread_cal_service,
                                calendar_id=user.calendar_id,
                                event_id=personal_google_event_id,
                            )
                            Log().log_info(f"Deleted personal google event for {session})")
                        except googleapiclient.errors.HttpError as e:
                            if e.status_code == 403:
                                Log().log_warning(
                                    f"Google_action_processor: Create - Google API error for {session}: {e}. Will try again later.")
                                failed = True

                    elif next_action == "update-participant":
                        user_id = arguments[0]
                        user: User = User.query.get(user_id)
                        if not user:
                            continue
                        session_user: Sessions_Users = Sessions_Users.query.filter(
                            and_(Sessions_Users.user_id == user_id, Sessions_Users.session_id == session.id)).first()
                        if not session_user:
                            # Association has been removed during delay.
                            continue
                        if not user.calendar_id or not session.personal_google_event_id:
                            self.enqueue_google_user_action(action=next_action, object_id=session_id,
                                                            arguments=arguments, front=True)
                            continue
                        try:
                            google_calendar.update_event(
                                service=this_thread_cal_service,
                                calendar_id=user.calendar_id,
                                event_id=session_user.personal_google_event_id,
                                session=session
                            )
                            Log().log_info(f"Updated personal google event for {session})")
                        except googleapiclient.errors.HttpError as e:
                            if e.status_code == 403:
                                Log().log_warning(
                                    f"Google_action_processor: Create - Google API error for {session}: {e}. Will try again later.")
                                failed = True

                    if failed:
                        # Retry the same action after a delay.
                        self.enqueue_google_user_action(action=next_action, object_id=session_id, arguments=arguments, front=True)
                        Log().log_warning(f"Google_action_processor: Failed action '{next_action}' for '{session}'. Will retry in {self.google_action_sleep} seconds.")

            if not failed:
                if self.google_action_sleep > config.MIN_GOOGLE_ACTION_SLEEP:
                    self.google_action_sleep /= 2
            else:
                if not (self.google_action_sleep >= config.MAX_GOOGLE_ACTION_SLEEP):
                    self.google_action_sleep *= 2
            time.sleep(self.google_action_sleep)

    @reconnect_on_fail
    def send_rabbitmq_response(self, response_template, action, arguments):
        """
        Send a response to the UUID manager after an action completed.
        @reconnect_on_fail decorator will attempt to recreate the rabbitMQ connection if it fails.
        :param action: Action performed. One of created, deleted, updated.
        :param response_template: String template used to respond. str.format is used to add arguments.
        :param arguments: arguments used in the template.
        """
        body = response_template.format(*arguments, action)
        self.channel.basic_publish(
            exchange='',
            routing_key=config.UUID_QUEUE,
            body=body,
        )


def subscribe_session_consumers():
    """
    Starts the consumer in a background thread.
    """
    session_consumer = SessionConsumer()
    thread = Thread(target=session_consumer.start_consumer, daemon=True)
    thread.start()
