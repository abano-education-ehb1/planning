import functools
import time
import traceback
from queue import Queue
from threading import Thread

import googleapiclient.errors
import pika
import pika.exceptions
import config
import google_calendar
import inititalisation
from consumers import Consumer
from helpers import dictionary_from_xml, xml_from_string, find_xml_subtree, xml_to_string, validate_xml, escape_for_xml, \
    reconnect_on_fail
from logger import Log

from models import db
from models.user import User
from lxml import etree

# TODO: No longer has a state so making this a class is not required. Might change it later when i'm sure.

uuid_manager_response_template = """
<response>
  <source>planning</source>
  <source-id>{}</source-id>
  <uuid>{}</uuid>
  <entity>{}</entity>
  <action>{}</action>
</response>
"""

calendar_link_response_template = """
<calendarlink>
    <source>planning</source>
    <entity>calendar</entity>
    <userID>{}</userID>
    <link-html>"{}"</link-html>
    <link-ics>"{}"</link-ics>
</calendarlink>
"""


class UserConsumer(Consumer):
    def __init__(self):
        super().__init__()
        # Search for users without and google calendar ID and tries to create them again.
        if config.USE_GOOGLE:
            with inititalisation.app.app_context():
                failed_user_calendars = User.query.filter(User.calendar_id == None).all()
                for failed_user in failed_user_calendars:
                    Log().log_info(f"{failed_user} has no calendar. Will retry now.")
                    self.enqueue_google_user_action(action="create", object_id=failed_user.id)
            Thread(target=self.start_google_action_processor, daemon=True).start()

    def start_consumer(self):
        """
        Creates the rabbitMQ connection and starts consuming. Blocks the current thread
        """
        while True:
            # Based on https://pika.readthedocs.io/en/stable/examples/blocking_consume_recover_multiple_hosts.html
            try:
                self.setup_rabbitmq_connection()
                self.channel.start_consuming()
            except (pika.exceptions.ConnectionClosedByBroker, pika.exceptions.AMQPConnectionError, pika.exceptions.AMQPChannelError) as e:
                Log().log_error(f"RabbitMQ connection closed on queue {config.USER_QUEUE_NAME}: {e}. Retrying connection in 5 seconds.")
                time.sleep(5)

    def setup_rabbitmq_connection(self):
        """
        Creates the rabbitMQ connection. Can also be used to recreate the connections if it breaks.
        """
        Log().log_info(f'Creating new connection to queue {config.USER_QUEUE_NAME}')
        connection = pika.BlockingConnection(config.rabbitmq_parameters)
        self.channel = connection.channel()
        self.channel.queue_declare(queue=config.USER_QUEUE_NAME)
        self.channel.basic_consume(on_message_callback=self.callback, queue=config.USER_QUEUE_NAME, auto_ack=True)

    def callback(self, channel, method, properties, body):
        """
        Function is called every time data arrives on the current queue.
        :param body: content of the rabbitMQ message (in our case the XML)
        """
        if isinstance(body, str):
            data = body
        else:
            data = body.decode()
        if "<source>planning</source>" in data:
            # Ignore when queue receives data sent from itself.
            return
        try:
            xml, _ = xml_from_string(data)
        except Exception as e:
            Log().log_error(f"UserConsumer.callback: Could not parse xml={data}")
            return
        is_valid = validate_xml(xml)
        if not is_valid:
            Log().log_error(f'UserConsumer.callback: xml is not valid.\n{xml_to_string(xml)}')
            return

        action = find_xml_subtree(xml, 'action', only_first=True).text
        if action == "create":
            return self.create_user(xml=xml)
        elif action == "delete":
            return self.delete_user(xml=xml)
        elif action == "update" in data:
            return self.update_user(xml=xml)

    def delete_user(self, xml):
        """
        Deletes a user given the xml data.
        :param xml: xml containing data about the user deletion
        """
        with inititalisation.app.app_context():
            try:
                # user_id = find_xml_subtree(xml, 'planning', only_first=True).text
                entity = xml.tag
                uuid = find_xml_subtree(xml, 'uuid', only_first=True).text
                user_instance: User = User.query.filter(User.uuid == uuid).first()
                # Check if user exists in database
                if not user_instance:
                    Log().log_error("User not found")
                    return
                if user_instance.calendar_id and config.USE_GOOGLE:
                    self.enqueue_google_user_action(action='delete', object_id=None, arguments=[user_instance.calendar_id])

                for assoc in user_instance.sessions:
                    db.session.delete(assoc)
                db.session.delete(user_instance)
                db.session.commit()
                Log().log_info(f"Deleted user: {user_instance}")
                if self.channel:
                    self.send_rabbitmq_response(response_template=uuid_manager_response_template,
                                                action="deleted",
                                                arguments=[user_instance.id, uuid, entity])
            except Exception as e:
                Log().log_error(f'UserConsumer.delete_user: Some exception occurred. {e}')


    def update_user(self, xml) -> User:
        """
        Updates a user given the xml data
        :param xml: xml containing data for updating the user
        """
        with inititalisation.app.app_context():
            try:
                entity = xml.tag
                if entity == "user":
                    firstname = find_xml_subtree(xml, 'firstname', only_first=True).text
                    lastname = find_xml_subtree(xml, 'lastname', only_first=True).text
                    username = firstname + " " + lastname
                else:
                    # entity == company.
                    username = find_xml_subtree(xml, 'name', only_first=True).text

                uuid = find_xml_subtree(xml, 'uuid', only_first=True).text
                email = find_xml_subtree(xml, 'email', only_first=True).text
                user_query: User = User.query.filter(User.uuid == uuid)
                # Check if user exists in database
                user_instance: User = user_query.first()  # Converts a query to a User instance.
                if not user_instance:
                    Log().log_error("User not found")
                    return
                if user_instance.username != username or user_instance.email != email:
                    user_query.update({
                        'email': email,
                        'username': username
                    })
                    db.session.commit()
                    if user_instance.calendar_id and config.USE_GOOGLE:
                        self.enqueue_google_user_action(action="update", object_id=user_instance.id, arguments=[username])

                Log().log_info(f"Updated user: {user_instance}")
                if self.channel:
                    self.send_rabbitmq_response(response_template=uuid_manager_response_template,
                                                action="updated",
                                                arguments=[user_instance.id, uuid, entity])
                return user_instance
            except Exception as e:
                Log().log_error(f'UserConsumer.update_user: Some exception occurred. {e}')

    def create_user(self, xml) -> User:
        """
        Creates a user given the xml data
        :param xml: xml data contains the data to create a new user
        :return: user
        """
        with inititalisation.app.app_context():
            try:
                entity = xml.tag
                if entity == "user":
                    try:
                        firstname = find_xml_subtree(xml, 'firstname', only_first=True).text
                        lastname = find_xml_subtree(xml, 'lastname', only_first=True).text
                        username = firstname + " " + lastname
                    except Exception as e:
                        print(f"Temporary fix for {e}")
                        username = firstname if firstname else "None" + " " + lastname if lastname else "None"
                else:
                    # entity == company.
                    username = find_xml_subtree(xml, 'name', only_first=True).text

                uuid = find_xml_subtree(xml, 'uuid', only_first=True).text
                email = find_xml_subtree(xml, 'email', only_first=True).text
                user = User(uuid=uuid,
                            username=username,
                            email=email,
                            particulier=True if entity == "user" else False)
            except Exception as e:
                Log().log_error(f'UserConsumer.create_user: Some exception occurred. {e}: {xml_to_string(xml)}')
            else:
                db.session.add(user)
                db.session.commit()
                Log().log_info(f'Created new user: {user}')
                if self.channel:
                    # During testing there is no rabbitmq channel. Only send response if channel != None
                    self.send_rabbitmq_response(response_template=uuid_manager_response_template,
                                                action="created",
                                                arguments=[user.id, uuid, entity])

                if config.USE_GOOGLE:
                    self.enqueue_google_user_action(action="create", object_id=user.id)
                return user


    def start_google_action_processor(self):
        """
        Loops over action in the google_action_queue and performs them with a delay between them.
        The delay will be doubled when an action failed and halved when it succeeds.
        Should be run in a background thread.
        # TODO: Needs refactoring.
        """
        this_thread_cal_service, this_thread_gmail_service = google_calendar.get_calendar_service()
        # The Google services are not thread safe. Always make a new service in each thread.
        while True:
            failed = False
            if self.google_action_queue:
                with inititalisation.app.app_context():
                    next_action, user_id, arguments = self.google_action_queue.popleft()
                    user = None
                    if user_id:
                        user: User = User.query.get(user_id)
                    if next_action == "create" and user:
                        try:
                            user_calendar = google_calendar.create_user_calendar(this_thread_cal_service,
                                                                                 user.username)
                        except googleapiclient.errors.HttpError as e:
                            if e.status_code == 403:
                                Log().log_warning(
                                    f"Google_action_processor: Create - Google API error for {user}: {e}. Will try again later.")
                                failed = True
                        else:
                            user.calendar_id = user_calendar['id']
                            db.session.commit()
                            self.enqueue_google_user_action(action='public', object_id=user.id, front=True)
                            calendar_link_html = f'https://calendar.google.com/calendar/embed?src={user.calendar_id}&ctz=Europe%2FBrussels'
                            calendar_link = google_calendar.create_calendar_link(user.calendar_id)
                            #if self.channel:
                            #    self.send_calendar_links(user_id=user.id, html_link=calendar_link_html, ics_link=calendar_link)
                            if config.USE_GOOGLE:
                                self.enqueue_google_user_action(action='mail', object_id=user.id, arguments=[calendar_link], front=True)
                            Log().log_info(f"Created Calendar for {user} has been created.")

                    elif next_action == "mail":
                        calendar_link = arguments[0]
                        google_calendar.send_google_calendar_link_email(
                            service=this_thread_gmail_service,
                            to=user.email,
                            link=calendar_link
                        )

                    elif next_action == "update" and user:
                        username = arguments[0]
                        try:
                            google_calendar.update_user_name(this_thread_cal_service, user.calendar_id, username)
                            Log().log_info(f"Updated Calendar for {user} has been updated.")
                        except googleapiclient.errors.HttpError as e:
                            if e.status_code == 403:
                                Log().log_warning(
                                    f"Google_action_processor: Create - Google API error for {user}: {e}. Will try again later.")
                                failed = True
                    elif next_action == "delete":
                        try:
                            calendar_id = arguments[0]
                            google_calendar.delete_calendar(this_thread_cal_service, calendar_id)
                            Log().log_info(f"Deleted Calendar for user with (ID={user_id}) has been deleted.")
                        except googleapiclient.errors.HttpError as e:
                            if e.status_code == 403:
                                Log().log_warning(
                                    f"Google_action_processor: Create - Google API error for {user}: {e}. Will try again later.")
                                failed = True
                    elif next_action == "public":
                        try:
                            google_calendar.make_calendar_public(this_thread_cal_service, user.calendar_id)
                            Log().log_info(f"Made calendar public for {user} has been made public.")
                        except googleapiclient.errors.HttpError as e:
                            if e.status_code == 403:
                                Log().log_warning(
                                    f"Google_action_processor: Create - Google API error for {user}: {e}. Will try again later.")
                                failed = True

                    if failed:
                        # Retry the same  action after a delay.
                        self.enqueue_google_user_action(action=next_action, object_id=user_id, arguments=arguments, front=True)
                        Log().log_info(
                            f"Google_action_processor: Failed action '{next_action}' for '{user}'. Will retry in {self.google_action_sleep} seconds.")

            if not failed:
                if self.google_action_sleep > config.MIN_GOOGLE_ACTION_SLEEP:
                    self.google_action_sleep /= 2
            else:
                if not self.google_action_sleep >= config.MAX_GOOGLE_ACTION_SLEEP:
                    self.google_action_sleep *= 2
            time.sleep(self.google_action_sleep)


    @reconnect_on_fail
    def send_rabbitmq_response(self, response_template, action, arguments):
        """
        Send a response to the UUID manager after an action completed.
        @reconnect_on_fail decorator will attempt to recreate the rabbitMQ connection if it fails.
        :param action: Action performed. One of created, deleted, updated.
        :param response_template: String template used to respond. str.format is used to add arguments.
        :param arguments: arguments used in the template.
        """
        body = response_template.format(*arguments, action)
        self.channel.basic_publish(
            exchange="",
            routing_key=config.UUID_QUEUE,
            body=body,
        )

    @reconnect_on_fail
    def send_calendar_links(self, user_id, html_link, ics_link):
        """
        Sends the calendar links of a given user to rabbitMQ.
        @reconnect_on_fail decorator will attempt to recreate the rabbitMQ connection if it fails.
        :param user_id: user_id
        :param html_link: html link for embedded calendar.
        :param ics_link: ics file download link
        """
        body = calendar_link_response_template.format(user_id, escape_for_xml(html_link), escape_for_xml(ics_link))
        self.channel.basic_publish(
            exchange="",
            routing_key=config.UUID_QUEUE,
            body=body,
        )




def subscribe_user_consumers():
    """
    Starts the consumer in a background thread.
    """
    user_consumer = UserConsumer()
    thread = Thread(target=user_consumer.start_consumer, daemon=True)
    thread.start()
