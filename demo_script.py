import datetime
import time
import uuid
from threading import Thread

import config
import inititalisation
import google_calendar
from consumers.sessions_consumer import SessionConsumer
from consumers.user_creation_consumer import UserConsumer
from google_calendar import get_calendar_service
from helpers import datetime_to_str, prettify_xml, xml_to_string, xml_from_string
from logger import Log
from models import db
from tests.unittests.utils import company_xml_template, user_xml_template, session_xml_template, user_session_join_template

if __name__ == '__main__':
    db.init_app(inititalisation.app)
    with inititalisation.app.app_context():
        db.create_all()
        print("Created database tables.")

    config.USE_GOOGLE = True
    service, gmail_service = get_calendar_service()
    user_consumer = UserConsumer()
    Thread(target=user_consumer.start_consumer, daemon=True).start()
    session_consumer = SessionConsumer()
    Thread(target=session_consumer.start_consumer, daemon=True).start()
    time.sleep(2)



    user_uuid = str(uuid.uuid4())
    # user_uuid = "c3f13878-9cf9-4767-a05b-a0fa8e1156fc"

    user = user_consumer.callback(
        channel=None, method=None, properties=None,
        body=user_xml_template.format(
            id="",
            uuid=user_uuid,
            action="create",
            firstname="Testje",
            lastname="Van Arthur",
            email="arthur.demo@gmail.com")
    )
    #
    # user_consumer.callback(
    #     channel=None, method=None, properties=None,
    #     body=user_xml_template.format(
    #         id=1,
    #         uuid=user_uuid,
    #         action="update",
    #         firstname="Service Thread 2",
    #         lastname="Demo",
    #         email="arthur.demo@gmail.com")
    # )


    # company_uuid = str(uuid.uuid4())
    # company_user = user_consumer.callback(
    #     channel=None, method=None, properties=None,
    #     body=company_xml_template.format(
    #         uuid=company_uuid,
    #         action="create",
    #         name="Microsoft",
    #         email="arthur.demo@gmail.com")
    # )

    # print(user_xml_template.format(
    #         uuid=user_uuid,
    #         action="create",
    #         firstname="Arthur",
    #         lastname="Demo",
    #         email="arthur.demo@gmail.com"))

    # --------------------------------------------------------------------------------------------------------------- #
    # time.sleep(1)
    session_uuid = str(uuid.uuid4())
    session_uuid = "0799b7c8-1ac7-49ce-9c8b-fb682fb8bb4a"

    # session = session_consumer.callback(
    #     channel=None, method=None, properties=None,
    #     body=session_xml_template.format(
    #         id="",
    #         action="create",
    #         uuid=session_uuid,
    #         name="Final Test",
    #         begin=int(time.time()),
    #         end=int(time.time() + 3600 * 4),
    #         speaker="Bob",
    #         location="A1",
    #         description="Demo session description."
    #     ))

    # session_consumer.callback(
    #     channel=None, method=None, properties=None,
    #     body=session_xml_template.format(
    #         id=1,
    #         action="delete",
    #         uuid=session_uuid,
    #         name="DELAYED UPDATE 3",
    #         begin=int(time.time()),
    #         end=int(time.time() + 3600 * 4),
    #         speaker="Bob",
    #         location="A1",
    #         description="Demo session description updated."
    #     ))

    # --------------------------------------------------------------------------------------------------------------- #
    # time.sleep(1)

    # session_consumer.callback(
    #     channel=None, method=None, properties=None,
    #     body=user_session_join_template.format(
    #         action="create",
    #         user_id=45,
    #         session_id=session.id
    #     ))

    # session_consumer.callback(
    #     channel=None, method=None, properties=None,
    #     body=user_session_join_template.format(
    #         action="delete",
    #         user_id=45,
    #         session_id=45
    #     ))

    input("Type something to stop demo script:\n")