import base64
import datetime
import os.path
import time
from email.mime.text import MIMEText
from pprint import pprint

import googleapiclient.http
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow, Flow
from googleapiclient.discovery import build
from google.oauth2 import service_account

import models.session
from config import ROOT_DIR, GOOGLE_EMAIL
from logger import Log
from models import user
import httplib2
import google_auth_httplib2
import requests
import google.auth
import google.auth.credentials


SCOPES = [
    'https://www.googleapis.com/auth/calendar',
    'https://www.googleapis.com/auth/calendar.events',
    'https://mail.google.com/'
]
# MAIN_CALENDAR = 'Integration Project Global'
MAIN_CALENDAR_ID = 'primary'  # Primary calendar is the name of the account by default.
EMAIL_ADDRESS = GOOGLE_EMAIL





def get_google_credentials():
    """
    Creates the authentication for the Google api. Can be reused multiple times.
    required credentials.json to be present and will generate a 'token.json' file.
    The first time it will open a browser window to confirm that you trust this project.
    If no browser is available, copy the token.json from the documentation. See README.md
    :return: google credentials object.
    """
    credentials = None
    if os.path.exists(os.path.join(ROOT_DIR, "token.json")):
        #Log(use_rabbitmq=False).log_info("Loading google credentials from token")
        credentials = Credentials.from_authorized_user_file(os.path.join(ROOT_DIR, 'token.json'), SCOPES)
    if not credentials or not credentials.valid:
        if credentials and credentials.expired and credentials.refresh_token:
            print("Refreshing google API credentials.")
            credentials.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                os.path.join(ROOT_DIR, 'credentials.json'),
                SCOPES
            )
            credentials = flow.run_local_server(port=0)
        # Save the credentials for the next run

        with open(os.path.join(ROOT_DIR, 'token.json'), 'w') as token:
            token.write(credentials.to_json())
    #Log(use_rabbitmq=False).log_info(f"Google credentials loaded. Created on {credentials.expiry}")
    return credentials


def get_google_credentials_service_account():
    """
    BROKEN: Don't user it now. Google account is not configured correctly.
    Should be used instead of "get_google_credentials" to authenticate using a service account.
    This allows authentication for server application without an available browser.
    """
    credentials = service_account.Credentials.from_service_account_file(
        "service-account.json", scopes=SCOPES)
    delegated_credentials = credentials.with_subject('integrationproject22@gmail.com')
    return delegated_credentials


def get_calendar_service(credentials=None):
    """
    Creates a connection to the google api. Service can be re-used multiple times to.
    :param credentials: authentication credentials.
    :return: calendar api connection.
    """
    try:
        if not credentials:
            credentials = get_google_credentials()

        # Create a new Http() object for every request
        def build_request(http, *args, **kwargs):
            new_http = google_auth_httplib2.AuthorizedHttp(credentials, http=httplib2.Http())
            return googleapiclient.http.HttpRequest(new_http, *args, **kwargs)

        authorized_http = google_auth_httplib2.AuthorizedHttp(credentials, http=httplib2.Http())
        cal_service = build('calendar', 'v3', requestBuilder=build_request, http=authorized_http)
        authorized_http = google_auth_httplib2.AuthorizedHttp(credentials, http=httplib2.Http())
        gmail_service = build('gmail', 'v1', requestBuilder=build_request, http=authorized_http)
        return cal_service, gmail_service
    except Exception as e:
        Log(use_rabbitmq=False).log_info(f"Could not create calendar service. This can be ignored during testing. {e}")
        return None, None



def create_event(service, calendar_id, session: models.session.Session):
    # Docs: https://developers.google.com/calendar/api/guides/create-events#python
    if not calendar_id:
        Log().log_error(f"google_calendar.create_event: calendar_id was None")
        return
    event_data = session.to_google_event_dict()
    event = service.events().insert(calendarId=calendar_id, body=event_data, quotaUser=calendar_id).execute()
    return event


def update_event(service, calendar_id, event_id, session: models.session.Session):
    if not calendar_id:
        Log().log_error(f"google_calendar.update_event: calendar_id was None")
        return
    if not event_id:
        Log().log_error(f"google_calendar.update_event: event_id was None")
        return
    event_data = session.to_google_event_dict()
    event = service.events().update(calendarId=calendar_id, eventId=event_id, body=event_data, quotaUser=calendar_id).execute()
    return event


def delete_event(service, calendar_id, event_id):
    if not event_id:
        Log().log_error(f'google_calendar.delete_event: Cannot delete event that that not exist on google.')
        return
    service.events().delete(calendarId=calendar_id, eventId=event_id, quotaUser=calendar_id).execute()


def create_user_calendar(service, calendar_name):
    """
    Creates a calendar for the user on Google calendar.
    :param service: google calendar service connection
    :param calendar_name: name to give to the Google calendar.
    """
    calendar_data = {
        'summary': calendar_name,
        'timeZone': 'Europe/Brussels'
    }
    created_calendar = service.calendars().insert(body=calendar_data).execute()

    return created_calendar


def remove_orphaned_calendars():
    # TODO: During development a lot of calendars will be created without associated users. Remove them regularly
    return


def get_user_calendar(service, calendar_id):
    """
    Gets the user calendar from the Google api.
    :param service: google calendar service connection
    :param calendar_id: google calendar id
    :return: dictionary with calendar data.
    """
    if not calendar_id:
        Log().log_error(f'google_calendar.get_user_calendar: calendar_id was None.')
    calendar = service.calendars().get(calendarId=calendar_id, quotaUser=calendar_id).execute()
    return calendar


def update_user_name(service, calendar_id, username):
    # API doc: https://developers.google.com/calendar/api/v3/reference/events/update
    if not calendar_id:
        Log().log_error(f"google_calendar.update_user: calendar_id was None")
        return
    calendar = service.calendars().get(calendarId=calendar_id).execute()
    calendar['summary'] = username
    updated_calendar = service.calendars().update(calendarId=calendar_id, body=calendar).execute()
    return updated_calendar


def delete_all_calendars(service):
    calendars = service.calendarList().list().execute()
    for calendar in calendars['items']:
        try:
            service.calendars().delete(calendarId=calendar['id']).execute()
            time.sleep(2)
        except Exception as e:
            if e.reason == 'Cannot delete a primary calendar. Use the clear method to clear it of all data instead.':
                service.calendars().clear(calendarId=calendar['id']).execute()


def delete_calendar(service, calendar_id):
    if not calendar_id:
        Log().log_error(f"google_calendar.delete_user: calendar_id was None")
        return
    service.calendars().delete(calendarId=calendar_id).execute()


def create_calendar_link(calendar_id: str):
    if not calendar_id:
        Log().log_error(f'google_calendar.create_calendar_link: calendar_id was None.')
        return None
    id = calendar_id.split("@")[0]
    link  = "https://calendar.google.com/calendar/ical/{}%40group.calendar.google.com/public/basic.ics".format(id)
    Log().log_info(f'Created public calendar url for {calendar_id}: {link}')
    return link


def make_calendar_public(service, calendar_id):
    if not calendar_id:
        Log().log_error(f'google_calendar.create_calendar_link: calendar_id was None.')
        return None
    rule = {
        "role": "reader",
        "scope": {
            "type": "default"
        }
    }
    service.acl().insert(calendarId=calendar_id, body=rule).execute()


def gmail_create_message(sender, to, subject, message_text):
    # Source: https://mailtrap.io/blog/send-emails-with-gmail-api/
    message = MIMEText(message_text)
    message['to'] = to
    # message['from'] = sender
    message['subject'] = subject
    raw_message = base64.urlsafe_b64encode(message.as_string().encode("utf-8"))
    return {
        'raw': raw_message.decode("utf-8")
    }


def gmail_send_message(service, to, subject, message):
    try:
        # create gmail api client
        message = gmail_create_message(to=to, sender=EMAIL_ADDRESS, subject=subject, message_text=message)
        message = service.users().messages().send(userId='me', body=message).execute()
        Log().log_info(f'Sent an email to "{to}" about "{subject}"')
    except Exception as error:
        Log().log_error(f'Could not send email to {to}: {error}')


def send_google_calendar_link_email(service, to, link):
    global_calendar = 'https://calendar.google.com/calendar/ical/integrationproject22%40gmail.com/public/basic.ics'  # Should not be hardcoded.
    gmail_send_message(
        service=service,
        to=to,
        subject="Your personal calendar for DHack has been created.",
        message=f"Thank you for visiting DHack. \n"
                f"In order to stay up to date with any changes to the schedule we have create a personalised calendar for you. \n"
                f"You can download the .ics file from the following link and import it in your preferred calendar application. \n"
                f"Personal Link: {link} \n"
                f"You can also import the general DHack calendar containing all sessions.\n"
                f"General Link: {global_calendar}\n"
                f"If presented with a 404 error, try again in a minute.\n\n"
                f"Greetings,\n"
                f"The DHack team."

    )


if __name__ == '__main__':
    service, gmail_service = get_calendar_service()
    delete_all_calendars(service=service)
    # send_google_calendar_link(
    #     gmail_service,
    #     EMAIL_ADDRESS,
    #     create_calendar_link(service.calendarList().list().execute()['items'][-1]['id'])
    # )
