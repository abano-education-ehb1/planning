import os
from threading import Lock
from xml.dom import minidom
from xml.etree import ElementTree
import datetime
from lxml import etree
import pika
import pika.exceptions
import time
from config import ROOT_DIR


class SingletonMeta(type):
    # Source: https://refactoring.guru/design-patterns/singleton/python/example#example-1
    """
    This is a thread-safe implementation of Singleton.
    """
    _instances = {}

    _lock: Lock = Lock()
    """
    We now have a lock object that will be used to synchronize threads during
    first access to the Singleton.
    """
    def __call__(cls, *args, **kwargs):
        """
        Possible changes to the value of the `__init__` argument do not affect
        the returned instance.
        """
        # Now, imagine that the program has just been launched. Since there's no
        # Singleton instance yet, multiple threads can simultaneously pass the
        # previous conditional and reach this point almost at the same time. The
        # first of them will acquire lock and will proceed further, while the
        # rest will wait here.
        with cls._lock:
            # The first thread to acquire the lock, reaches this conditional,
            # goes inside and creates the Singleton instance. Once it leaves the
            # lock block, a thread that might have been waiting for the lock
            # release may then enter this section. But since the Singleton field
            # is already initialized, the thread won't create a new object.
            if cls not in cls._instances:
                instance = super().__call__(*args, **kwargs)
                cls._instances[cls] = instance
        return cls._instances[cls]


def datetime_to_str(dtime: datetime.datetime, format: str) -> str:
    """
    Converts datetime objects to the format the Google api uses.
    :param dtime: datetime.datetime object to be converted.
    :return:
    """
    return dtime.strftime(format)


def escape_for_xml(str):
    s = str.replace("&", "&amp;")
    s = s.replace("<", "&lt;")
    s = s.replace(">", "&gt;")
    s = s.replace("\"", "&quot;")
    return s


def string_to_datetime(datetime_string: str, format: str) -> datetime.datetime:
    return datetime.datetime.strptime(datetime_string, format)


def timestamp_to_datetime(ts: int):
    date_time = datetime.datetime.fromtimestamp(ts)
    return date_time


def dictionary_from_xml(xml: etree):
    # TODO: Seems "hacky". There should be a better way to do this.
    # IMPORTANT: Can not have duplicate keys.
    data_dict = {}
    for element in xml.iter():
        data_dict[element.tag] = element.text
    return data_dict


def xml_from_string(xml_string, validate=True, schema_file=None) -> etree:
    """
    Parses xml from a string. Also validates it if a validation schema is provided.
    :param xml_string: string representation of xml.
    :param validate: Validate the xml.
    :param schema_file: file path to schema to use. If None one will be chosen automatically.
    :return: lxml.etree and optionally validation. is_valid can be True, False or None is no validation was done.
    """
    xml = etree.fromstring(xml_string)
    is_valid = None
    if validate:
        is_valid = validate_xml(xml_tree=xml, schema_file=schema_file)
    return xml, is_valid


def find_xml_subtree(xml: etree, tag: str, only_first=False):
    elements = list(xml.getiterator(tag))
    if only_first:
        return elements[0]
    return elements


def validate_xml(xml_tree: etree, schema_file = None):
    if not schema_file:
        schema_file = get_validation_file(xml_tree.tag)
    xml_validator = etree.XMLSchema(file=schema_file)
    return xml_validator.validate(xml_tree)


def prettify_xml(element_tree):
    """Return a pretty-printed XML string for the Element. """
    rough_string = ElementTree.tostring(element_tree, 'unicode')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")


def xml_to_string(xml: etree):
    try:
        return ElementTree.tostring(xml, encoding='unicode')
    except:
        # XML is malformed and can not be parsed.
        return str(xml)


def get_validation_file(entity: str) -> str:
    return os.path.join(ROOT_DIR, f'resources/xml_validators/{entity}.xsd')


def reconnect_on_fail(function):
    """
    decorator that attempt to recreate the RabbitMQ connection if it fails.
    """
    import logger


    def wrap(self, *args, **kwargs):
        try:
            return function(self, *args, **kwargs)
        except (pika.exceptions.ConnectionClosedByBroker, pika.exceptions.AMQPConnectionError,
                pika.exceptions.AMQPChannelError) as e:
            logger.Log().log_error(f"RabbitMQ connection when sending: {e}. Retrying connection in 5 seconds.")
            time.sleep(5)
            self.setup_rabbitmq_connection()
            return function(self, *args, **kwargs)
    return wrap
