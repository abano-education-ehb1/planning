from flask import Flask
from config import DB_HOST, DB_PORT, DB_USER, DB_PASSWORD, DATABASE_NAME
import google_calendar
from models import db

# google_credentials = google_calendar.get_google_credentials()
google_calendar_service, gmail_service = google_calendar.get_calendar_service()

app = Flask(__name__)
# Config doc: https://flask-sqlalchemy.palletsprojects.com/en/2.x/config/
database_connection_string = f"mariadb+mariadbconnector://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DATABASE_NAME}"
print("DB Connection String:", database_connection_string)
app.config['SQLALCHEMY_DATABASE_URI'] = database_connection_string
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False  # Only needed when using SQLAlchemy events.
