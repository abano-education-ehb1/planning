import queue
import sys
import time
import datetime
from threading import Thread
import logging
import pika
import config
import helpers

error_xml_template = """
<error>
    <source>planning</source>
    <date>{date}</date>
    <level>{level}</level>
    <message>{message}</message>
</error>
"""


class Log(metaclass=helpers.SingletonMeta):
    logger = logging.getLogger(__name__)
    log_queue: queue.Queue = queue.Queue()
    send_to_monitoring = False
    enabled = True
    connection = None
    channel = None

    def __init__(self, use_rabbitmq=True):
        self.enable()
        handler = logging.StreamHandler(sys.stdout)
        handler.setFormatter(logging.Formatter(config.LOGGER_FORMAT))
        self.logger.addHandler(handler)

        if use_rabbitmq:
            self.send_to_monitoring = True
            self.setup_rabbitmq_connection()
            self.log_info("Started Logger Publisher....")
            Thread(target=self.start_sending_log, daemon=True).start()

    def setup_rabbitmq_connection(self):
        self.log_info(f'Creating new logger connection...')
        self.connection = pika.BlockingConnection(config.rabbitmq_parameters)
        self.channel = self.connection.channel()
        self.channel.queue_declare(queue='errors')

    def disable(self):
        self.logger.setLevel(logging.NOTSET)
        self.enabled = False

    def enable(self):
        self.logger.setLevel(logging.INFO)
        self.enabled = True

    def log_error(self, message):
        self.logger.error("ERROR -  %s", message)
        self.log_queue.put(("error", message))

    def log_warning(self, message):
        self.logger.warning("WARNING - %s", message)
        self.log_queue.put(("warn", message))

    def log_critical(self, message):
        self.logger.critical("CRITICAL -  %s", message)
        self.log_queue.put(("fatal", message))

    def log_info(self, message):
        # Info is logged but not send to monitoring.
        self.logger.info(message)

    def start_sending_log(self):
        while self.send_to_monitoring and self.channel:
            if self.enabled and not self.log_queue.empty():
                level, message = self.log_queue.get()
                error_xml = error_xml_template.format(
                    date=int(time.time()),
                    level=level,
                    message=helpers.escape_for_xml(message)
                )
                try:
                    _, is_valid = helpers.xml_from_string(error_xml)
                    if not is_valid:
                        self.log_error(f"Log.start_sending_log: Tried to send invalid error xml. {error_xml}")
                    else:
                        self.send_error(error_xml)
                except Exception as e:
                    print(e)
                    logging.error(f"Error send error xml: { error_xml}",)
            else:
                time.sleep(1)

    @helpers.reconnect_on_fail
    def send_error(self, error_xml):
        self.channel.basic_publish(
            exchange='',
            routing_key='errors',
            body=str(error_xml),
        )
