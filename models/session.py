from helpers import datetime_to_str
from models import db


# participants_assoc = db.Table(
#     'session_users', db.metadata,
#     db.Column('session_id', db.Integer, db.ForeignKey('session.id'), primary_key=True),
#     db.Column('user_id', db.Integer, db.ForeignKey('user.id'), primary_key=True),
#     db.Column('personal_google_event_id', db.String, db.ForeignKey('user.id'), primary_key=True)
# )


class Sessions_Users(db.Model):
    __tablename__ = 'session_users'
    session_id = db.Column(db.ForeignKey('session.id', ondelete="CASCADE"), primary_key=True)
    user_id = db.Column(db.ForeignKey('user.id', ondelete="CASCADE"), primary_key=True)
    user = db.relationship("User", back_populates="sessions")
    session = db.relationship("Session", back_populates="participants")
    personal_google_event_id = db.Column(db.String(255), unique=False, nullable=True)


class Session(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    uuid = db.Column(db.String(80), unique=False, nullable=False)
    name = db.Column(db.String(255), unique=False, nullable=False)
    begin_time = db.Column(db.DateTime(), unique=False, nullable=False)
    end_time = db.Column(db.DateTime(), unique=False, nullable=False)
    speaker = db.Column(db.String(255), unique=False, nullable=True)
    location = db.Column(db.String(255), unique=False, nullable=True)
    description = db.Column(db.String(255), unique=False, nullable=True)
    google_event_id = db.Column(db.String(255), unique=False, nullable=True)
    participants = db.relationship('Sessions_Users',
                                   back_populates='session')

    def add_participant(self, user: 'User', personal_google_event_id: str = None) -> Sessions_Users:
        """
        :param user: User object to add.
        :param personal_google_event_id: google calendar ID if one has been created.
        :return: association object between Users table and Sessions table.
        Is NOT committed to the database automatically. Commit it after this method is called.
        """
        return Sessions_Users(
            user=user,
            session=self,
            personal_google_event_id=personal_google_event_id,
        )

    def remove_participant(self, session_user: Sessions_Users):
        self.participants.remove(session_user)
        db.session.delete(session_user)

    def to_google_event_dict(self):
        return {
            'summary': self.name,
            'location': self.location,
            'description': (f"Speaker: {self.speaker}\n\n{self.description}" if self.speaker not in [None, "nan"] else ""),
            'start': {
                'dateTime': datetime_to_str(self.begin_time, '%Y-%m-%dT%H:%M:%S%z'),
                'timeZone': 'Europe/Brussels',
            },
            'end': {
                'dateTime': datetime_to_str(self.end_time, '%Y-%m-%dT%H:%M:%S%z'),
                'timeZone': 'Europe/Brussels',
            },
        }

    def __repr__(self):
        return f'<Session({self.name}) by {self.speaker}]>'
