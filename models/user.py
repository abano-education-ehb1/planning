from models import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    uuid = db.Column(db.String(80), unique=False, nullable=False)
    username = db.Column(db.String(255), unique=False, nullable=False)
    email = db.Column(db.String(255), unique=False, nullable=False)
    particulier = db.Column(db.Boolean, nullable=False)
    calendar_id = db.Column(db.String(80), unique=False, nullable=True)
    sessions = db.relationship(
        "Sessions_Users",
        back_populates="user"
    )

    def __repr__(self):
        return f'<User(UUID={self.uuid}) {self.username} ({"particulier" if self.particulier else "bedrijf"})]>'
