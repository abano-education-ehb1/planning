import datetime
import time
from threading import Thread
import pika
import config
from helpers import xml_from_string, reconnect_on_fail
from logger import Log

monitoring_send_xml = """
<heartbeat>
    <source>planning</source>
    <date>{}</date>
</heartbeat>
"""


class MonitoringPublisher(object):
    channel = None

    def setup_rabbitmq_connection(self):
        Log().log_info("Creating new heartbeat connection.")
        connection = pika.BlockingConnection(config.rabbitmq_parameters)
        self.channel = connection.channel()
        self.channel.queue_declare(queue=config.HEARTBEAT_QUEUE)

    def start_publishing(self, start_delay):
        time.sleep(start_delay)
        self.setup_rabbitmq_connection()
        Log().log_info("Started Monitoring Publisher....")

        while True:
            now_time = int(time.time())
            xml_data = monitoring_send_xml.format(now_time).strip()
            _, is_valid = xml_from_string(xml_data)
            if not is_valid:
                Log().log_error(f"MonitoringPublisher.start_publishing: heartbeat format is not valid.\n{xml_data}")
                return
            self.send_heartbeat(xml_data)
            time.sleep(3)

    @reconnect_on_fail
    def send_heartbeat(self, xml_data):
        if not self.channel:
            Log().log_error("Cannot send heartbeat because channel == None")
            return
        self.channel.basic_publish(
            exchange="",
            routing_key=config.HEARTBEAT_QUEUE,
            body=xml_data,
        )


def start_monitoring_publisher(start_delay=0):
    """
    Starts the publisher in a background thread.
    """
    publisher = MonitoringPublisher()
    thread = Thread(target=publisher.start_publishing, args=(start_delay, ), daemon=True)
    thread.start()


if __name__ == '__main__':
    ...
    # publisher = MonitoringPublisher()
    # publisher.start_publishing(0)
