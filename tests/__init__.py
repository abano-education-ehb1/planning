import unittest
import config
from logger import Log


class PlanningTestCase(unittest.TestCase):
    def __init__(self, *args):
        Log(use_rabbitmq=False).disable()
        config.USE_GOOGLE = False
        super().__init__(*args)

