import os

from flask import Flask

from models import db
import models.user
import models.session

_basedir = os.path.abspath(os.path.dirname(__file__))


class SetupTestingDatabase(object):

    @staticmethod
    def create_app(app=None):
        """
        Overwrites a flask db config with another config used for testing.
        """
        if not app:
            app = Flask(__name__)
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        db.init_app(app)
        with app.app_context():
            db.create_all()
        return app.test_client()
