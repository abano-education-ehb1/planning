import datetime
import time
import unittest
import uuid

from flask.testing import FlaskClient as FlaskTestClient

from helpers import timestamp_to_datetime
from models import db
from models.session import Session, Sessions_Users
from models.user import User
from tests import PlanningTestCase
from tests.unittests.setup_database import SetupTestingDatabase
from tests.unittests.utils import create_test_session, create_test_user


class TestDatabase(PlanningTestCase):
    shared_app: FlaskTestClient = None

    @classmethod
    def setUpClass(cls):
        cls.shared_app = SetupTestingDatabase.create_app()

    def test_user_model(self):
        with self.shared_app.application.app_context():
            # Add user, session and join the session.
            user = create_test_user('test_user_model')
            db.session.add(user)
            session = create_test_session('test_user_model', int(time.time()), int(time.time() + 3600 * 4))
            db.session.add(session)
            session.add_participant(user)
            db.session.commit()
            # Test if relationships are configured correctly and bidirectional.
            database_user = User.query.get(user.id)
            database_session = Session.query.get(session.id)
            self.assertEqual(database_user.sessions, database_session.participants)


    def test_participant_join_and_leave(self):
        with self.shared_app.application.app_context():
            user = create_test_user('test_participant_join_and_leave')
            db.session.add(user)
            session = create_test_session('test_participant_join_and_leave', int(time.time()), int(time.time() + 3600 * 4))
            db.session.add(session)
            session.add_participant(user)
            db.session.commit()

            self.assertEqual(len(session.participants), 1)
            self.assertEqual(session.participants, user.sessions)

            session.remove_participant(user.sessions[0])
            db.session.commit()
            self.assertEqual(len(session.participants), 0)




    @classmethod
    def tearDownClass(cls):
        # TODO: Maybe close the database connection? Does it required with an in-memory database?
        ...


if __name__ == '__main__':
    unittest.main()
