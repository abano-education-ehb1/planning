import time
import unittest
import uuid
from flask.testing import FlaskClient as FlaskTestClient, FlaskClient
import inititalisation
from consumers.sessions_consumer import SessionConsumer
from consumers.user_creation_consumer import UserConsumer
from demo_script import user_session_join_template
from models import db
from models.session import Session
from tests import PlanningTestCase
from tests.unittests.setup_database import SetupTestingDatabase
from tests.unittests.utils import create_test_user, create_test_session, session_xml_template


class TestRabbitMQSessions(PlanningTestCase):
    shared_app: FlaskTestClient = None

    @classmethod
    def setUpClass(cls):
        cls.shared_app = SetupTestingDatabase.create_app(app=inititalisation.app)

    def setUp(self):
        self.session_consumer = SessionConsumer()
        self.user_consumer = UserConsumer()
        self.test_session = self.create_session(session_uuid=str(uuid.uuid4()), name="unittest")


    def create_session(self, session_uuid, name):
        return self.session_consumer.callback(
            channel=None, method=None, properties=None,
            body=session_xml_template.format(
                id="",
                action="create",
                uuid=session_uuid,
                name=name,
                begin=int(time.time()),
                end=int(time.time() + 3600 * 4),
                speaker="unittest",
                location="unittest",
                description="unittest"
            )
        )

    def test_session_create(self):
        session = self.create_session(session_uuid=str(uuid.uuid4()), name="unittest_create")
        self.assertIsNotNone(session)

    def test_session_update(self):
        session_xml = session_xml_template.format(
            id=self.test_session.id,
            action="update",
            uuid=self.test_session.uuid,
            name="unittest updated",
            begin=int(time.time()),
            end=int(time.time() + 3600 * 4),
            speaker="unittest",
            location="unittest",
            description="unittest"
        )
        update_session = self.session_consumer.callback(
            channel=None, method=None, properties=None,
            body=session_xml
        )
        self.assertIsNotNone(update_session)
        self.assertEqual(update_session.name, "unittest updated")

    def test_session_delete(self):
        session_id = self.test_session.id
        session_uuid = self.test_session.uuid
        self.session_consumer.callback(
            channel=None, method=None, properties=None,
            body=session_xml_template.format(
                id=self.test_session.id,
                action="delete",
                uuid=self.test_session.uuid,
                name="",
                begin=0,
                end=0,
                speaker="",
                location="",
                description="")
        )
        with self.shared_app.application.app_context():
            database_session = Session.query.get(session_id)
            self.assertIsNone(database_session)

    def test_user_session_join(self):
        with self.shared_app.application.app_context():
            user = create_test_user('unittest')
            session = create_test_session('unittest', int(time.time()), int(time.time() + 3600 * 4))
            db.session.add(user)
            db.session.add(session)
            db.session.commit()

            user_session_xml = user_session_join_template.format(
                action='create',
                session_id=session.id,
                user_id=user.id
            )

            session_user = self.session_consumer.callback(
                channel=None, method=None, properties=None,
                body=user_session_xml
            )
            self.assertIsNotNone(session_user)


if __name__ == '__main__':
    unittest.main()
