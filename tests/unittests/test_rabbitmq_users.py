import os
import sys
import unittest
import uuid
from pprint import pprint
from unittest import mock
from unittest.mock import MagicMock

import pika
from lxml import etree

# from consumers.user_creation_consumer import UserConsumer
import config
import inititalisation
from consumers.user_creation_consumer import UserConsumer
from helpers import xml_from_string, dictionary_from_xml, find_xml_subtree
from logger import Log
from models.user import User
from tests import PlanningTestCase
from tests.unittests.setup_database import SetupTestingDatabase
from flask.testing import FlaskClient as FlaskTestClient, FlaskClient
from models import db
from tests.unittests.utils import user_xml_template


class TestRabbitMQUsers(PlanningTestCase):
    shared_app: FlaskTestClient = None

    @classmethod
    def setUpClass(cls):
        cls.shared_app = SetupTestingDatabase.create_app(app=inititalisation.app)

    def setUp(self):
        self.user_consumer = UserConsumer()
        self.test_user = self.create_user(user_uuid=str(uuid.uuid4()), firstname="unittest", lastname="unittest")

    def create_user(self, user_uuid, firstname, lastname):
        from tests.unittests.utils import user_xml_template
        return self.user_consumer.callback(
            channel=None, method=None, properties=None,
            body=user_xml_template.format(
                id=0,
                uuid=user_uuid,
                action="create",
                firstname=firstname,
                lastname=lastname,
                email="unittest@mail.com")
        )

    def test_user_create(self):
        user = self.create_user(user_uuid=str(uuid.uuid4()), firstname="unittest_create", lastname="unittest_create")
        self.assertIsNotNone(user)

    def test_user_update(self):
        user_xml = user_xml_template.format(
            id=self.test_user.id,
            uuid=self.test_user.uuid,
            action="update",
            firstname="updated",
            lastname="updated",
            email="unittest.updated@mail.com")
        update_user = self.user_consumer.callback(
            channel=None, method=None, properties=None,
            body=user_xml
        )
        self.assertIsNotNone(update_user)
        self.assertEqual(update_user.username, "updated updated")
        self.assertEqual(update_user.email, "unittest.updated@mail.com")

    def test_user_delete(self):
        user_id = self.test_user.id
        user_uuid = self.test_user.uuid
        update_user = self.user_consumer.callback(
            channel=None, method=None, properties=None,
            body=user_xml_template.format(
                id=self.test_user.id,
                uuid=self.test_user.uuid,
                action="delete",
                firstname="",
                lastname="",
                email="")
        )
        with self.shared_app.application.app_context():
            database_user = User.query.get(user_id)
            self.assertIsNone(database_user)




if __name__ == '__main__':
    unittest.main()
