import os
import unittest
from pprint import pprint

from lxml import etree

from helpers import xml_from_string, dictionary_from_xml, find_xml_subtree
from tests import PlanningTestCase


class TestXML(PlanningTestCase):

    def setUp(self):
        self.xml_string = """
            <root>
               <element key='value'>text</element>
               <element>text</element>
               <empty-element/>
            </root>
        """
        self.validation_schema = os.path.join(os.path.dirname(__file__), "xml_test.xsd")

    def test_xml_from_string(self):
        xml_tree, _ = xml_from_string(self.xml_string, validate=False)
        xml_elements = list(xml_tree)
        self.assertTrue(xml_elements[0].tag == 'element')
        self.assertTrue(xml_elements[0].attrib == {'key': 'value'})
        self.assertTrue(xml_elements[1].tag == 'element')
        self.assertTrue(xml_elements[2].tag == 'empty-element')


    def test_validate_xml(self):
        _, is_valid = xml_from_string(self.xml_string, validate=True, schema_file=self.validation_schema)
        self.assertTrue(is_valid)

    def test_find_xml_subtree(self):
        xml_tree, _ = xml_from_string(self.xml_string, schema_file=self.validation_schema)
        element_results = find_xml_subtree(xml_tree, 'element')
        self.assertEqual(len(element_results), 2)
        first_element = find_xml_subtree(xml_tree, 'element', only_first=True)
        self.assertEqual(first_element.attrib, {'key': 'value'})



if __name__ == '__main__':
    unittest.main()
