import uuid

from helpers import timestamp_to_datetime
from models.session import Session
from models.user import User


company_xml_template = """
<company>
  <source>frontend</source>
  <source-id>0</source-id>
  <uuid>{uuid}</uuid>
  <action>{action}</action>
  <properties>
    <name>{name}</name>
    <email>{email}</email>
    <address>
      <street>string</street>
      <housenumber>4635</housenumber>
      <postalcode>1802</postalcode>
      <city>string</city>
      <country>string</country>
    </address>
    <phone>string</phone>
    <taxId>string</taxId>
  </properties>
</company>
"""

user_xml_template = """
<user>
  <source>frontend</source>
  <source-id>{id}</source-id>
  <uuid>{uuid}</uuid>
  <action>{action}</action>
  <properties>
    <firstname>{firstname}</firstname>
    <lastname>{lastname}</lastname>
    <email>{email}</email>
    <address>
      <street>string</street>
      <housenumber>4635</housenumber>
      <postalcode>1802</postalcode>
      <city>string</city>
      <country>string</country>
    </address>
    <phone>string</phone>
    <company>
      <source-id>string</source-id>
      <uuid>string</uuid>
    </company>
  </properties>
</user>
"""


session_xml_template = """
<session>
  <source>frontend</source>
  <action>{action}</action>
  <source-id>{id}</source-id>
  <uuid>{uuid}</uuid>
  <properties>
    <name>{name}</name>
    <beginDateTime>{begin}</beginDateTime>
    <endDateTime>{end}</endDateTime>
    <speaker>{speaker}</speaker>
    <location>{location}</location>
    <description>{description}</description>
  </properties>
</session>
"""

user_session_join_template = """
<user-session>
  <source>frontend</source>
  <sessionId>{session_id}</sessionId>
  <sessionName>session_name</sessionName>
  <userId>{user_id}</userId>
  <action>{action}</action>
  <email>email</email>
</user-session>
"""


def create_test_user(name) -> User:
    return User(
        uuid=str(uuid.uuid4()),
        username=f'{name}',
        email=f"{name}@email.com",
        particulier=True
    )


def create_test_session(name, begin: int, end: int) -> Session:
    return Session(
        uuid=str(uuid.uuid4()),
        name=f"{name}",
        description=f"{name}_description",
        begin_time=timestamp_to_datetime(begin),
        end_time=timestamp_to_datetime(end),
        speaker=f"{name}_speaker",
        location=f"{name}_location",
        google_event_id=None
    )